import cherrypy
import time 
import json
import threading,socket
import os
import requests


s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
s.connect(("8.8.8.8",80))
localIp = s.getsockname()[0]
socketPort = 60001
serviceCatalogUrl = 'http://192.168.1.19:8080/'


class CustomError(Exception):
	def __init__(self, message):
		Exception.__init__(self)
		self.message = message

class OnlineCatalog():
    exposed = True
    def __init__(self, catalogID, broker, port, usersFilePath):
        self.catalogID = catalogID
        self.usersFilePath = usersFilePath
        self.menuGET = {'get userID': self.getUser }
        self.menuPUTPOST = {'add userID': self.addUser}
        self.broker = { 'broker': broker, 'port': port }
        try:
            with open(usersFilePath, 'r') as source:
                self.usersList = json.load(source)['usersList']
                source.close()
        except FileNotFoundError:
            with open(usersFilePath, 'w') as source:
                json.dump({'usersList': [] }, source, indent = 4)
                source.close()
            self.usersList = []


    def GET (self, *uri, **param):
        res = list(uri)
        command = ' '.join(uri)
        try:
            output = self.menuGET[command](list(param.values())[0])
        except KeyError:
            output = 'Wrong request'
        except IndexError:
            output = self.menuGET[command]()
        return output 
    def POST (self, *uri, **param):
        res = list(uri)
        command = ' '.join(uri)
        payload = json.loads(cherrypy.request.body.read())
        try:
            output = self.menuPUTPOST[command](payload, True)
        except KeyError:
            output = 'Wrong request'
        except CustomError as error:
            output = error.message
        return output
    def PUT (self, *uri, **param):
        res = list(uri)
        command = ' '.join(uri)
        payload = json.loads(cherrypy.request.body.read())
        try:
            output = self.menuPUTPOST[command](payload, False)
        except KeyError:
            output = 'Wrong request'
        return output 
    def getUser(self, userID = None):
        found = 'UserID not found'
        if userID != None:
            for i in range(len(self.usersList)):
                if self.usersList[i]['userID'] == userID:
                    found = json.dumps(self.usersList[i], indent = 4)
        else:
            found = json.dumps(self.usersList, indent = 4)
        return found
    def addUser(self, *data):
        try:
            userData = data[0]
            isPOST = data[1]
            output = 'User added' 
            for i in range(len(self.usersList)):
                if self.usersList[i]['userID'] == userData['userID']:
                    if isPOST:
                        raise CustomError('userIs is not available')
                    else:
                        self.usersList.pop(i)
                        output = 'User found and updated'
                        break
            self.usersList.append(userData)
            with open(self.usersFilePath, 'w') as source:
                json.dump({ "usersList": self.usersList }, source)
                source.close()
        except CustomError:
            pass
        
        return output 
	
class RegistrationThread(threading.Thread):
    def __init__(self, threadID, serviceCatalogUrl, deviceCatalogUrl):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.serviceCatalogUrl = serviceCatalogUrl
        self.catalogInformation = dict(serviceID='user_database', url= deviceCatalogUrl)
    def run(self):
        while True:
            time.sleep(5)
            try:
                r = requests.put(self.serviceCatalogUrl + 'register serviceID', data = json.dumps(self.catalogInformation) )
                r.raise_for_status()
            except http_error:
                print('No Service Catalog connection')
                break
	

class refreshThread(threading.Thread):
	def __init__(self, threadID):
		threading.Thread.__init__(self)
		self.threadID = threadID
	def run(self):
		while keepAlive:
			time.sleep(20)
			
			value = len(catalog.deviceList)
			print(f'{value}')
			for i in range(len(catalog.deviceList)):
				if time.time()-catalog.deviceList[i]['insert-timestamp'] > 20:
					catalog.deviceList.pop(i)
					break
			print(f'{json.dumps(catalog.deviceList, indent = 4)}')


if __name__=='__main__':
    print(f'{os.getpid()}')
  
    r = requests.get(serviceCatalogUrl+"get broker")
    config = json.loads(r.text)
    thread2 = RegistrationThread('thread2', serviceCatalogUrl,f"http://{localIp}:{socketPort}/" )
    catalog = OnlineCatalog('catalog1', config['broker'], config['port'],  'users.txt')
    conf = {
		'/':{
				'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
				'tools.sessions.on': True
		}
	}
    cherrypy.config.update({'server.socket_host': localIp,'server.socket_port':socketPort})

    cherrypy.tree.mount(catalog,'/', conf)
    cherrypy.engine.start()
    thread2.start()
    cherrypy.engine.block()