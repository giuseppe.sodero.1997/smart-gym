#passare url service catalog da qui e modificare rm.py
#gestione errori seria
#codici risposta http
#chiusura stanze non ricevute nell'od config

from signal import pause
from rm import *
import json,socket
import threading
import time
import cherrypy,requests

s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
s.connect(("8.8.8.8",80))
localIp = s.getsockname()[0]
socketPort = 8099
url_srvCat = 'http://192.168.1.9:8080/'



class CustomError(Exception):
	def __init__(self, message):
		Exception.__init__(self)
		self.message = message

class refreshThread(threading.Thread):
    def __init__(self, threadID):
        threading.Thread.__init__(self)
        self.threadID = threadID
    def run(self):
        while True:
            time.sleep(5)
            try:
                url_SRM = f'http://{localIp}:{socketPort}'
                srvId = 'Specific Room Management'
                registration_payload = dict(serviceID=srvId,url=url_SRM)
                r = requests.put(url_srvCat + "register serviceID", data=json.dumps(registration_payload))    
            except:
                print('Connection issue')
                time.sleep(5)

class SpecificRoomManagement():
    exposed = True
    def __init__(self,triggerObj):
        self.triggerObj = triggerObj
  
    def POST (self, *uri, **param):
        payload = json.loads(cherrypy.request.body.read())
        print(payload)
        try:
            output = self.triggerObj.updateRooms(payload)#, True,time.time())
        except KeyError:
            output = 'Wrong request'
        return output

    def PUT (self, *uri, **param):
        payload = json.loads(cherrypy.request.body.read())
        try:
            output = self.triggerObj.updateRooms(payload)#, False,time.time())
        except KeyError:
            output = 'Wrong request'
        return output

class globalManager():
    def __init__(self):
        self.activeRooms = []

    def updateRooms(self,odConf):
        #aggiungere distinzione tra POST,PUT?
        returnList=[]
        roomInfo={}
        for gym in odConf['gymList']:
            for room in gym['roomList']:
                roomInfo = room
                roomInfo['roomName'] = roomInfo.pop('name')
                roomInfo['gymName'] = gym['name']
                roomInfo['url_srvCat'] = url_srvCat
                roomId = f'{roomInfo["gymName"]}/{roomInfo["roomName"]}'
                if roomId in [item.get('id') for item in self.activeRooms]:
                    item = [item for item in self.activeRooms if item.get('id')==roomId][0]
                    item['obj'].stop()
                    self.activeRooms.remove(item)
                tmpDict = {}
                tmpDict['id'] = roomId
                tmpDict['obj'] = roomManager(roomInfo)
                tmpDict['obj'].start()
                self.activeRooms.append(tmpDict)
                returnList.append(f'{roomId} created!<br>')
        return returnList
                    
if __name__ == '__main__':


    # Initialization
    mainManager = globalManager() 
    srvCatReg = refreshThread('srvCatReg')
    srvCatReg.start()

    # Cherrypy configuration
    conf = {
		'/':{
				'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
				'tool.session.on': True 
		}
	}
    cherrypy.config.update({'server.socket_host':localIp})
    cherrypy.config.update({'server.socket_port':socketPort})
    cherrypy.tree.mount(SpecificRoomManagement(mainManager),'/', conf)
    cherrypy.engine.start()