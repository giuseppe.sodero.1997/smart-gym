import paho.mqtt.client as PahoMQTT
import json

class MyMQTT:
  
    def __init__(self, clientID, broker, port,notifier=None):#catalogUrl, notifier=None):
        self.clientID = clientID
        self.broker = broker
        self.port = port
        self.notifier = notifier
        self._topicManager = []
        #self.catalogUrl = catalogUrl

        # Instantiation of paho.mqtt.client
        self._paho_mqtt = PahoMQTT.Client(self.clientID,False)
        
        # Callback definition 
        self._paho_mqtt.on_connect = self.myOnConnect
        self._paho_mqtt.on_message = self.myOnMessageReceived

    def myOnConnect(self, client, userdata, flags, rc):
        if rc == 0: print(f'{self.clientID} connected to {self.broker}')
        else: print(f'Connection error number: {rc}')

    def myOnMessageReceived(self, client, userdata, msg):
        #if self.notifier == None: return # !!! MODIFICATO, RIMUOVI DOPO I TEST 1/12
        self.notifier.notify(msg.topic,msg.payload)

    def myPublish(self, topic, msg):
        print(f'Publishing {msg} with topic {topic}')
        self._paho_mqtt.publish(topic, json.dumps(msg),2)

    def mySubscribe(self, topic):
        print(f'Subrscibing to {topic}...')
        self._paho_mqtt.subscribe(topic,2)
        self._topicManager.append([topic,True])

    def myUnsubscribe(self,topic):
        print(f'Unsubscribing from {topic}')
        self._paho_mqtt.unsubscribe(topic)
        for item in self._topicManager:
            if item[0] == topic: self._topicManager.remove(item)

    def start(self):
        self._paho_mqtt.connect(self.broker,self.port)
        self._paho_mqtt.loop_start()

    def stop(self):
        for item in self._topicManager:
            if item[1] == True: self._paho_mqtt.unsubscribe(item[0])
        self._paho_mqtt.loop_stop()
        self._paho_mqtt.disconnect()
        