from myMQTT import *
import json
import time
import requests
import threading

url_servicecatalog = 'http://127.0.0.1:8080/'
class CustomError(Exception):
	def __init__(self, message):
		Exception.__init__(self)
		self.message = message

class EntryControl():
    def __init__(self,clientID, broker,port,maximum,serviceCatalog_url):
        self.client=MyMQTT(clientID,broker,port,self)
        self.roomDict = {}
        self.defaultMax = maximum
        self.serviceCatalog_url = serviceCatalog_url
        self.lock = threading.Lock()
    def start(self):
        self.client.start()
        print("---DOOR CONTROL STARTED---")
        while True:
            #entering critical section
            self.lock.acquire()
            r = requests.get(self.serviceCatalog_url+'get/serviceID?serviceID=Device+Catalog')
            if r.text != 'serviceID not found': 
                deviceCatalog = json.loads(r.text)
                r = requests.get(deviceCatalog['url']+'get/deviceID')
                deviceList = json.loads(r.text)
                for device in deviceList:
                    if device['deviceID'][0:8:]=='RFIDdoor':
                        if device['topic'] not in self.client.myGetTopics():
                            self.client.mySubscribe(device['topic'])
                            self.roomDict[device['topic']] = dict(threshold = self.defaultMax, users = [])
            else:
                print('Waiting for Device Catalog')
            self.lock.release()
            #exit critical section
            time.sleep(10)
                     
    def stop(self):
        self.client.stop()
        
        
    def notify(self,topic,msg):
        data = json.loads(msg)
        #entering critical section
        self.lock.acquire()
        try:
            

            if len(self.roomDict[topic]['users'])<self.roomDict[topic]['threshold']:
                if data['e']['from'] != "":
                    tmp = json.dumps(self.roomDict[data['e']['from']])
                    print(tmp)
                    if data['e']['RFIDdata'] not in self.roomDict[data['e']['from']]['users']:
                        raise CustomError
                    self.roomDict[data['e']['from']]['users'].remove(data['e']['RFIDdata'])
                    self.client.myPublish('INFO/'+data['e']['from'], dict(nUsersInRoom = len( self.roomDict[data['e']['from']]['users']), roomThreshold = self.roomDict[data['e']['from']]['threshold'] ) )

                self.roomDict[topic]['users'].append(data['e']['RFIDdata'])
                r = requests.get(self.serviceCatalog_url+'get/serviceID?serviceID=Device+Catalog')
                deviceCatalog = json.loads(r.text)
                r = requests.get(deviceCatalog['url']+'get/deviceID?deviceID='+data['e']['doorID'])
                device = json.loads(r.text)
                #command = dict(serviceID = 'entryControl', value = 1)
                command = dict(deviceID = device['deviceID'], value = 1)
                r = requests.put(device['url'],data = json.dumps(command))
                with open('roomList.txt','w') as fp:
                    json.dump(self.roomDict,fp,  indent = 4)
                    fp.close()
                self.client.myPublish('INFO/'+topic, dict( nUsersInRoom = len(self.roomDict[topic]['users']),  roomThreshold = self.roomDict[topic]['threshold']) )
            else:
                pass


        except KeyError:
            print('keyError')
        except CustomError:
            print('room travel error')
        finally:
            self.lock.release()
        #exit critical section
       
        
class refreshThread(threading.Thread):
    def __init__(self, threadID):
        threading.Thread.__init__(self)
        self.threadID = threadID
    def run(self):
        while True:
            time.sleep(5)
            registration_payload={
                "serviceID":"entry_control"   
            }
    
            r = requests.put(url_servicecatalog + "register serviceID", data=json.dumps(registration_payload))
        
                
if __name__ == "__main__":
    
    
    r = requests.get(url_servicecatalog + "get/broker")
    settings = r.text
    settings = json.loads(settings)
    print(settings)
    
    thread_reg = refreshThread("thread_reg")
    
    broker=settings["broker"]
    port=settings["port"]
    DS=EntryControl('entry control 1', broker,port,3,url_servicecatalog)
    thread_reg.start()
    DS.start()
    
    
 


    
    
    
    
