from urllib.parse import ParseResult
from urllib.request import CacheFTPHandler

from paho.mqtt.client import Client
from myMQTT import *
import json,socket
import time
import requests
import threading
import cherrypy

s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
s.connect(("8.8.8.8",80))
localIp = s.getsockname()[0]
socketPort = 60000
url_servicecatalog = 'http://192.168.1.19:8080/'

class CustomError(Exception):
	def __init__(self):
		Exception.__init__(self)
		

class EntryControl():
    def __init__(self,clientID, broker,port,maximum,serviceCatalog_url, entryControlurl):
        self.client=MyMQTT(clientID+'pippo',broker,port,self)
        self.roomDict = {}
        self.defaultMax = maximum
        self.serviceCatalog_url = serviceCatalog_url
        self.lock = threading.Lock()
        self.url = entryControlurl
    exposed = True
    def start(self):
        self.client.start()
        print("---DOOR CONTROL STARTED---")
        while True:
            #entering critical section
            self.lock.acquire()
            r = requests.get(self.serviceCatalog_url+'get/serviceID?serviceID=Device+Catalog')
            if r.text != 'serviceID not found': 
                deviceCatalog = json.loads(r.text)
                r = requests.get(deviceCatalog['url']+'get/deviceID')
                deviceList = json.loads(r.text)
                for device in deviceList:
                    if device['deviceID'][0:8:]=='RFIDdoor':
                        if device['topic'] not in self.client.myGetTopics():
                            self.client.mySubscribe(device['topic'])
                            self.roomDict[device['topic']] = dict(userThreshold = self.defaultMax, totalThreshold = self.defaultMax, type0 = [], type1 = [], type2 = [], type3 = [])
            else:
                print('Waiting for Device Catalog')
            self.lock.release()
            #exit critical section
            time.sleep(10)
    def GET(self, *uri, **param):
        command = ' '.join(uri)
        if command == 'get userID':
            print(param)
            room = self.roomDict.get(param['roomTopic'], None)
            if room != None:
                found = False
                for i in room['users']:
                    if i == param['userID']:
                        output = i
                        found = True
                if not found :
                    output = 'UserID not found'
            else:
                output = 'wrong topic'
        else: 
            output = 'wrong request'
        return output
    
    def POST(self, *uri, **param):
        command = ' '.join(uri)
        data = json.loads(cherrypy.request.body.read())
        print(data)
        print(self.roomDict)
        try :
            roomTopic = data.get('roomTopic', None)
            userThreshold = data.get('userThreshold', None)
            totalThreshold = data.get('totalThreshold', None)
            if roomTopic == None or  userThreshold == None or totalThreshold == None:
                raise CustomError
            if command == 'setThreshold':
                self.roomDict[roomTopic]['userThreshold'] = userThreshold
                self.roomDict[roomTopic]['totalThreshold'] = totalThreshold
            else:
                raise CustomError
            output = 'Update done'
        except CustomError:
            output = 'Wrong Paramaters in Payload'
        except KeyError:
            output = 'RoomTopic not defined'
        finally:
            return output 

    def stop(self):
        self.client.stop()
        
    def nPeople(self, roomDict):
        nUsers = len( roomDict['type0'])
        n1 = len( roomDict['type1'])
        n2 = len( roomDict['type2'])
        n3 = len( roomDict['type3'])
        peopleInRoom = nUsers + n1 + n2 + n3
        return peopleInRoom, nUsers,n1,n2,n3


    def notify(self,topic,msg):
        data = json.loads(msg)
        #entering critical section
        self.lock.acquire()
        try:
            ###
            #
            # rData = get('databaseurl', data = data.rfIDdata)
            # rData {rfIDdata = 'ziopino', type = 'user'}
            r = requests.get(self.serviceCatalog_url+'get/serviceID?serviceID=user_database')
            if r.text != 'serviceID not found': 
                userDatabase = json.loads(r.text)
            else:
                raise CustomError
            print(userDatabase)
            r = requests.get(userDatabase['url'] +'get/userID?userID='+data['e']['RFIDdata'])
            if r.text != 'UserID not found': 
                user = json.loads(r.text)
                type = 'type'+str(user['type'])
                print(type)
            else:
                raise CustomError
            print(self.roomDict[topic])
            peopleInRoom, nUsers, _, _, _ = self.nPeople(self.roomDict[topic])
            if  peopleInRoom < self.roomDict[topic]['totalThreshold']:
                if nUsers<self.roomDict[topic]['userThreshold'] or type != 'type0':
                    ### leaving a room 
                    if data['e']['from'] != "":
                        tmp = json.dumps(self.roomDict[data['e']['from']])
                        print(tmp)

                        if data['e']['RFIDdata'] not in self.roomDict[data['e']['from']][type]:
                                raise CustomError
                        self.roomDict[data['e']['from']][type].remove(data['e']['RFIDdata'])
                        
                        peopleInRoom, nUsers, n1, n2, n3= self.nPeople(self.roomDict[data['e']['from']])
                        userThresh = self.roomDict[data['e']['from']]['userThreshold']
                        roomThreshold = self.roomDict[data['e']['from']]['totalThreshold']
                        self.client.myPublish(data['e']['from']+'/INFO', dict(type0 = nUsers, type1 = n1, type2 = n2, type3 = n3, totalThreshold = roomThreshold, userThreshold = userThresh,  value = data['e']['RFIDdata'], isEntering = False,userType=type) )
                    ###leaving a room end
                    
                    if data['e']['RFIDdata']  in self.roomDict[topic][type]:
                            raise CustomError
                    self.roomDict[topic][type].append(data['e']['RFIDdata'])
                    
                    ###answer setup -entering room
                    
                    peopleInRoom, nUsers, n1, n2, n3= self.nPeople(self.roomDict[topic])
                    userThresh = self.roomDict[topic]['userThreshold']
                    roomThreshold = self.roomDict[topic]['totalThreshold']
                    ###answer setup -entering room end
                    r = requests.get(self.serviceCatalog_url+'get/serviceID?serviceID=Device+Catalog')
                    deviceCatalog = json.loads(r.text)
                    r = requests.get(deviceCatalog['url']+'get/deviceID?deviceID='+data['e']['doorID'])
                    device = json.loads(r.text)
                    #command = dict(serviceID = 'entryControl', value = 1)
                    command = dict(deviceID = device['deviceID'], value = 1)
                    r = requests.put(device['url'],data = json.dumps(command))
                    with open('roomList.txt','w') as fp:
                        json.dump(self.roomDict,fp,  indent = 4)
                        fp.close()
                    self.client.myPublish(topic+'/INFO', dict(type0 = nUsers, type1 = n1, type2 = n2, type3 = n3, totalThreshold = roomThreshold, userThreshold = userThresh,  value = data['e']['RFIDdata'], isEntering = True,userType=type)  )
            else:
                pass


        except KeyError:
            print('keyError')
        except CustomError:
            print('room travel error')
        finally:
            self.lock.release()
        #exit critical section
       
        
class refreshThread(threading.Thread):
    def __init__(self, threadID, client):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.client = client
    def run(self):
        while True:
            time.sleep(5)
            registration_payload={
                "serviceID":"entry_control",
                "url": self.client.url
            }
            r = requests.put(url_servicecatalog + "register serviceID", data=json.dumps(registration_payload))
        
                
if __name__ == "__main__":
    conf = {
        '/':{
            'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on':True
        }
    }
    cherrypy.config.update({'server.socket_host': localIp,'server.socket_port':socketPort})
    r = requests.get(url_servicecatalog + "get/broker")
    settings = r.text
    settings = json.loads(settings)
    print(settings)
    
    
    
    broker=settings["broker"]
    port=settings["port"]
    DS=EntryControl('entry control 1', broker,port,3,url_servicecatalog, f"http://{localIp}:{socketPort}/")
    thread_reg = refreshThread("thread_reg", DS)
    cherrypy.tree.mount(DS, '/', conf)
    cherrypy.engine.start()
    thread_reg.start()
    DS.start()
    cherrypy.engine.block()
    
    
 


    
    
    
    
