# This Python file uses the following encoding: utf-8
from myMQTT import *
import os, threading, requests
from pathlib import Path
import sys
import datetime
import json
import time
import cherrypy

from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine
from PySide2.QtCore import QObject, Slot, Signal, QTimer, QUrl



def colConv(num):
    if num == 0: return 'green'
    if num == 1: return 'orange'
    if num == 2: return 'grey'
    if num == 3: return 'red'
    return

class refreshThread(threading.Thread):
    def __init__(self, threadID):
        threading.Thread.__init__(self)
        self.threadID = threadID
    def run(self):
        while True:
            time.sleep(5)
            url_OD = f'http://{socket_ip}:{socket_port}/'
            srvId = 'Owner dashboard'
            registration_payload = dict(serviceID=srvId,url=url_OD)
            try:    
                r = requests.put(url_srvCat + "register serviceID", data=json.dumps(registration_payload))
            except:
                print('Connecton Error!')

class MainWindow(QObject,threading.Thread):
    def __init__(self):
        QObject.__init__(self)
        threading.Thread.__init__(self)

        # QTimer - Run Timer
        #self.timer = QTimer()
        #self.timer.timeout.connect(lambda: self.setTime())
        #self.timer.start(1000)
        
        self.srvCat = 'http://127.0.0.1:8080/'
        self.context = {'gym':None,'room':None,'menu':'home'}
        self.updateFlag = {'gyms':False,'rooms':False,'devices':False}
        self.displayLock = threading.Lock()


        # completo menu coca , poca cipolla, poco piccante
        # completo menu coca ricchione, poco piccante
        # completo menu coca
        # menu sprite kebab, patate, cipolla, lattuga salsa yogurt (solo maio nelle patatine)
        # 31

        # # Machine Control Configuration data
        # self.adj = {}
        # self.machines ={}  
    
        ## MQTT SECTION
        #da includere in un try con il resto della configurazione iniziale
        mqttConfig = requests.get(self.srvCat+"get broker").json()
        self.mqttClient = MyMQTT('mainListener', mqttConfig['broker'],mqttConfig['port'],self)
        self.mqttClient.start()

        self.dataStartup()

    # Ale signals
    
    updElem = Signal('QVariant')
    gymSig = Signal(list)
    roomSig = Signal(list)
    devSig = Signal(list)
    devListSig = Signal(int,list)
    adjSig = Signal(int,list)
    connStatus = Signal('QVariant')
    userSig = Signal('Qvariant')

    # Signal Set Data
    printTime = Signal(str)
    
    def start_engine(self):
        self.engine = QQmlApplicationEngine()
        self.start()

    def subscribeTo(self,topic):
        self.mqttClient.mySubscribe(topic)

    # Set Timer Function

    def setTime(self):
        now = datetime.datetime.now()
        formatDate = now.strftime("Now is %H:%M:%S of %Y/%m/%d")
        #print(formatDate)
        self.printTime.emit(formatDate)
        

            
    ################# ALE'S BACKEND ###############
    def run(self):
        while True:
            time.sleep(1)
            self.updateData()


    ##### FRONT-END METHODS #####
    @Slot(str,str,str,str)
    def pairList(self,gym,room,M1,M2):
        ###### RIMUOVERE LUCI,AUDIO ECC
        if gym == 'None':
            self.gymSig.emit([{'name':gym,'status':colConv(self.status[gym]['status'])}for gym in self.dataStructure])
        elif room == 'None':
            self.roomSig.emit([{'name':room,'status':colConv(self.status[gym][room]['status'])}for room in self.dataStructure[gym]])
        elif M1 == 'None': 
            self.devListSig.emit(1,[{'name':dev,'status':'green'}for dev in self.dataStructure[gym][room]])
        elif M2 == 'None':
            self.devListSig.emit(2,[{'name':dev,'status':'green'}for dev in self.dataStructure[gym][room] if dev != M1])
        else:
            self.updateMachine(gym,room,M1,M2)

    @Slot(str,str,str)
    def getAdjacency(self,gym,room,M1):

        if gym == 'None':
            self.adjSig.emit(1,[{'name':gym,'status':colConv(self.status[gym]['status'])}for gym in self.dataStructure])
        elif room == 'None':
            self.adjSig.emit(2,[{'name':room,'status':colConv(self.status[gym][room]['status'])}for room in self.dataStructure[gym]])
        elif M1 == 'None': 
            self.adjSig.emit(3,[{'name':dev,'status':'red'}for dev in self.dataStructure[gym][room]])
        else:
            m = self.adj.get(M1,None)
            if m == None:
                self.adjSig.emit(4,[])
            else:    
                self.adjSig.emit(4,[{'name':elem,'status':'green'}for elem in self.adj[M1]])

    @Slot(str,str)
    def retrieveData(self,gym,room):
        # This function is called by the onClicked method of gym/room elements
        # If room is 'None', the click is coming from a gym element which request roomList
        # otherwise it is coming from a room element which request devList
        #print(f'------->{gym},{room}')
        if room == 'None':
            #load rooms
            #ENTERING CRITICAL SECTION, ADD LOCKS
            
            self.roomSig.emit([{'name':room,'status':colConv(self.status[gym][room]['status'])} for room in self.dataStructure[gym]])  
            self.context['gym'] = gym
            self.context['room'] = None
            #RELEASE LOCK
        else:
            #load devices
            #ENTERING CRITICAL SECTION, ADD LOCKS
            self.devSig.emit([{'type':item['type'],'status':colConv(item['status'])} for item in self.dataStructure[gym][room].values()])
            self.context['gym'] = gym
            self.context['room'] = room
            #RELEASE LOCK 
    @Slot(str)
    def updateContext(self,selectedWindow):
        if selectedWindow == 'home':# and self.context['menu'] != 'home':
            self.context['gym'] = None
            self.context['room'] = None
            self.gymSig.emit([{'name':gym,'status':colConv(self.status[gym]['status'])}for gym in self.dataStructure])
            self.context['menu'] = 'home'
        else:
            self.context['menu'] = selectedWindow
            self.context['gym'] = None
            self.context['room'] = None
   
    @Slot('QVariant')
    def addUser(self,data):
        try:
            uDatabaseUrl = requests.get(self.srvCat+'get/serviceID?serviceID=user_database').json()['url']
        except:
            print('Something went wrong!')
            self.connStatus.emit({"status":"red","description":"Connection Error"})
            return
        response = requests.put(uDatabaseUrl + "add userID", data=data)
        print(response.text)

    @Slot()
    def getUsers(self):
        try:
            uDatabaseUrl = requests.get(self.srvCat+'get/serviceID?serviceID=user_database').json()['url']
        except:
            self.connStatus.emit({"status":"red","description":"Connection Error"})
            return
        response = requests.get(uDatabaseUrl + "get userID").json()
        #response is a list of dict
        self.userSig.emit(response)

   
   ##### BACK-END METHODS #####
    def notify(self,topic,msg):
        status = int(msg)
        li = topic.split('/')
        gym = li[1]
        room = li[2]
        devID = li[4]
        if self.context['menu'] == 'home' and self.context['gym'] == gym and self.context['room'] == room:
            self.dataStructure[gym][room][devID]['status'] = status
            idx = self.dataStructure[gym][room][devID]['idx']
            self.updElem.emit({'model':3,'idx':idx,'data':{'status':colConv(status)}})        
    
    def updateMachine(self,gym,room,M1,M2):
        if self.adj.get(M1,None) == None and self.adj.get(M2,None) == None: #Due nuove macchine
            self.adj[M1] = list()
            self.adj[M2] = list()
            self.adj[M1].append(M2)
            self.adj[M2].append(M1)
        elif self.adj.get(M1,None) == None: #M1 non presente, M2 presente
            self.adj[M1] = list()
            self.adj[M1].append(M2)
            self.adj[M2].append(M1)
        elif self.adj.get(M2,None) == None: #M2 non presente, M1 presente
            self.adj[M2] = list()
            self.adj[M1].append(M2)
            self.adj[M2].append(M1)
        
        fAdj = open('adj.json','w')
        json.dump(self.adj,fAdj)

        # AGGIORNAMENTO MACCHINE
        devCat = requests.get(self.srvCat+'get/serviceID?serviceID=Device+Catalog').json()['url']
        for machine in [M1,M2]:
            payload = dict(topics=[]) 
            for elem in self.adj[machine]:
                payload['topics'].append(f"IOT/{gym}/{room}/MACHINE/{elem}/STATUS") 
            mUrl = requests.get(f"{devCat}get/deviceID?deviceID={machine}").json()['address']
            requests.put(mUrl+'configuration',data=json.dumps(payload))
    
    def sortData(self):
        #Sorting gyms
        for gym in self.dataStructure:
            self.dataStructure = {k: v for k, v in sorted(self.dataStructure.items(), key=lambda item: item[0])}
        # Sorting rooms
        for gym in self.dataStructure:
            for room in self.dataStructure[gym]:
                    self.dataStructure[gym] = {k: v for k, v in sorted(self.dataStructure[gym].items(), key=lambda item: item[0])}
        ##################################################
        # Sorting devices
        for gym in self.dataStructure:
            for room in self.dataStructure[gym]:
                self.dataStructure[gym][room] = {k: v for k, v in sorted(self.dataStructure[gym][room].items(), key=lambda item: item[0])}
                for idx,dev in enumerate(self.dataStructure[gym][room]):
                    self.dataStructure[gym][room][dev]['idx'] = idx 

    def updateStatus(self,activeDevices):
        totalSet = set()
        freshSet = set()

        freshSet.update([device['deviceID'] for device in activeDevices])
        ## replace with compact form
        for gym in self.dataStructure:
            for room in self.dataStructure[gym]:
                totalSet.update(list(self.dataStructure[gym][room]))
        # ############################################

        unavailableDev = totalSet.difference(freshSet)      
        
        if len(unavailableDev) == 0: 
            for gym in self.status:
                for room in self.status[gym]:
                    if room != 'status':
                        self.status[gym][room]['status'] = 0
                        if self.context['gym'] == gym:
                            self.updElem.emit({'model':2,'idx':list(self.dataStructure[gym]).index(room),'data':{'status':'green'}})

                    else:
                        self.status[gym]['status'] = 0
                        if self.context['gym'] != None:
                            self.updElem.emit({'model':1,'idx':list(self.dataStructure).index(gym),'data':{'status':'green'}})

            return
        
        faultSet = set()
        
        #SISTEMARE STA CAGATA
        self.resetStatus()
        
        for missingDev in unavailableDev:
            for gym in self.dataStructure:
                for room in self.dataStructure[gym]:
                    retVal = self.dataStructure[gym][room].get(missingDev,None)
                    if  retVal!= None:
                        self.dataStructure[gym][room][missingDev]['status'] = 3
                        faultSet.add(gym+'/'+room)
                        if self.context['gym'] == gym and self.context['room'] == room:
                            self.updElem.emit({'model':3,'idx':retVal['idx'],'data':{'status':'red'}}) #da usare con model.set()

        
                       
        for item in faultSet:
            gym = item.split('/')[0]
            room = item.split('/')[1]
            self.status[gym]['status'] = 3
            self.status[gym][room]['status'] = 3
            self.updElem.emit({'model':1,'idx':list(self.dataStructure).index(gym),'data':{'status':'red'}})
            if self.context['gym'] == gym:
                self.updElem.emit({'model':2,'idx':list(self.dataStructure[gym]).index(room),'data':{'status':'red'}})

    def resetStatus(self):
        for gym in self.status:
                for room in self.status[gym]:
                    if room != 'status':
                        self.status[gym][room]['status'] = 0
                    else:
                        self.status[gym]['status'] = 0

    def updateData(self):
        # Retrieve active devices from device catalog
        try:
            devCat = requests.get(self.srvCat+'get/serviceID?serviceID=Device+Catalog').json()['url']
            activeDevices = requests.get(devCat+'get/deviceID?').json()
        except:
            self.connStatus.emit({"status":"red","description":"Connection Error"})
            return

        self.connStatus.emit({"status":"green","description":"Connected"})
        #print(f"active devices-->{[item['deviceID'] for item in activeDevices]}")
        
        # Set initially gym and room status to Fault, if the gym/room 
        # don't contains missing devices it will be updated in updateStatus() 
        for gym in self.status:
            for vGym in self.status[gym]:
                if vGym != 'status':
                    self.status[gym][vGym]['status'] = 3
                else:
                        self.status[gym]['status'] = 3

        if self.dataStructure != {}:
            self.updateStatus(activeDevices)

        for device in activeDevices:
            dGym = device['gymName']
            dRoom = device['roomName']
            dID = device['deviceID']
            
            gymVal = self.dataStructure.get(dGym,None)
            if gymVal == None: 
                self.status[dGym] = dict(status=0)
                self.dataStructure[dGym] = {}
                self.updateFlag['gyms'] = True

            roomVal = self.dataStructure[dGym].get(dRoom,None)
            if roomVal == None: 
                self.dataStructure[dGym][dRoom] = {}
                self.status[dGym][dRoom] = dict(status=0)
                if self.context['gym'] == dGym:
                    self.updateFlag['rooms'] = True

            devVal = self.dataStructure[dGym][dRoom].get(dID,None)
            
            if self.context['room'] == dRoom:
                if devVal == None:
                    self.updateFlag['devices'] = True
                else:
                    if devVal['status'] != 0:
                        self.updateFlag['devices'] = True

            self.dataStructure[dGym][dRoom][dID] = dict(type=device['type'],status=0,idx=None,description=device['description'],shordDesc=device['shortDesc'])

        self.sortData()
        fData = open('data.json','w')
        json.dump(self.dataStructure,fData)
        fData.close()
        # Checks if during data visualization there was an update inherent to 
        # selected visualization and eventually update it
        if self.context['menu'] != 'home':
            self.updateFlag['gyms'] = False
            self.updateFlag['rooms'] = False
            self.updateFlag['devices'] = False

        if True in self.updateFlag.values():
            if self.updateFlag['gyms'] == True: 
                lista = list(self.dataStructure.keys())
                l = []
                for item in lista:
                    l.append({'name':item,'status':'green'})
                self.gymSig.emit(l)
                self.context['menu'] = 'home'
                self.updateFlag['gyms'] = False

            if self.updateFlag['rooms'] == True: 
                lista = list(self.dataStructure[self.context['gym']].keys())
                r = []
                for item in lista:
                    r.append({'name':item,'status':'green'})
                self.roomSig.emit(r)
                self.updateFlag['rooms'] = False

            if self.updateFlag['devices'] == True: 
                lista = list(self.dataStructure[self.context['gym']][self.context['room']].keys())
                r = []
                for item in lista:
                    r.append({'type':item,'status':'green'})
                self.devSig.emit(r)
                self.updateFlag['devices'] = False

    def dataStartup(self):    
        ## Data load
        try:
            fData = open('data.json')
            self.dataStructure = json.load(fData)
            fData.close()
            self.updateFlag['gyms'] = True
        except: 
            self.dataStructure = {}
        
        try:
            fAdj = open('adj.json')
            self.adj = json.load(fAdj)
            fAdj.close()
        except: 
            self.adj = {}

        try:
            fMachines = open('machines.json')
            self.machines = json.load(fMachines)
            fMachines.close()
        except: 
            self.machines = {}

        for elem in self.machines.values():
            self.subscribeTo(elem+'/INFO')
        
        # Updating status
        self.status = {}
        for gym in self.dataStructure:
            self.status[gym] = dict(status=0)
            for room in self.dataStructure[gym]:
                self.status[gym][room] = dict(status=0)
        
        if self.context['menu'] == 'home' and len(self.dataStructure)>0:
                self.updateFlag['gyms'] = True

    
    @Slot()
    def goodBye(self):
        print('unsubscribing...')
        for elem in self.machines.values():
            self.mqttClient.unsubscribe(elem)

class infoDisp():
    def __init__(self,windowObj):
        self.windowObj = windowObj

    exposed = True
    def GET(self, *uri, **params):
        if len(uri) == 0: return json.dumps({"error":"no command found"})
        if uri[0] == 'machineConfiguration':
            deviceID = params.get('deviceID',None)
            machineTopic = params.get('machineTopic',None)
            if deviceID == None or machineTopic == None: return json.dumps({"error":"deviceID not present"})
            
            self.windowObj.machines[deviceID] = machineTopic
            self.windowObj.subscribeTo(machineTopic+'/INFO')
            fAdj = open('machines.json','w')
            json.dump(self.windowObj.machines,fAdj)
            fAdj.close()
            adjList = self.windowObj.adj.get(deviceID,None)
            if adjList == None:  return json.dumps({"error":"deviceID not present in adjacency list"})
            returnD = dict(topics=[])
            for adjMachine in adjList:
                returnD['topics'].append(f"{self.windowObj.machines[adjMachine]}/STATUS")
            print(returnD)
            return json.dumps(returnD)
        return 'Bad Request'
    

if __name__ == "__main__":

    url_srvCat = 'http://127.0.0.1:8080/'
    socket_port = 50095
    socket_ip = '127.0.0.1'
    
    app = QGuiApplication(sys.argv)
    app.setOrganizationName('pythonProj')
    app.setOrganizationDomain('OD')
    srvCatReg = refreshThread('srvCatReg')
    
    # Get Context
    main = MainWindow()
    main.start_engine()
    srvCatReg.start()

    # Cherrypy configuration
    conf = {
		'/':{
				'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
				'tool.session.on': True 
		}
	}
    cherrypy.config.update({'server.socket_port':50095})
    cherrypy.tree.mount(infoDisp(main),'/', conf)
    
	


    # Set Context
    main.engine.rootContext().setContextProperty("backend", main)
    
    # Load QML file
    main.engine.load(os.fspath(Path(__file__).resolve().parent / "qml/main.qml"))
    
    cherrypy.engine.start()
    if not main.engine.rootObjects():
        sys.exit(-1)
    

    sys.exit(app.exec_())

    
