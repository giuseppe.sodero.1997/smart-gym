import json



def scheduledConverter(threshold,schedDict):
    newDict = {}
    for elem in schedDict:
        tmp = {}
        hourString = elem.split(' ')
        if len(hourString) == 3:
            date,start,end = hourString
            startString = f"{date} {start}"
            endString = f"{date} {end}"
            tmp[startString] = schedDict[elem].copy()
            tmp[startString]['Start'] = True
            tmp[endString] = schedDict[elem].copy()
            tmp[endString]['Start'] = False
            for type in tmp[endString].keys():
                if tmp[endString][type] == True: tmp[endString][type] = False
                if type == 'Threshold': tmp[endString][type] = threshold
        else:
            date,start = hourString
            startString = f"{date} {start}"
            tmp[startString] = schedDict[elem].copy()
        newDict.update(tmp)

if __name__ == "__main__":
    scheduledConverter(25,{'20-2-2022 17:00:00 18:00:00': {'Threshold': 10, 'Audio': True, 'Light': True, 'Spinbike': True, 'TV': True}, '23-2-2022 21:00:00 22:00:00': {'Threshold': 10, 'Audio': True, 'Light': True, 'Spinbike': True, 'TV': True}, '19-2-2022 20:00:00': {'Threshold': 10, 'Audio': True, 'Light': True, 'Spinbike': True, 'TV': True}})