import cherrypy
import time 
import json
import threading
import os
class CustomError(Exception):
	def __init__(self, message):
		Exception.__init__(self)
		self.message = message

class ServiceCatalog():
	exposed = True
	def __init__(self, catalogID, broker, port, usersFilePath, lock):
		self.catalogID = catalogID
		self.serviceList = []
		self.usersFilePath = usersFilePath
		self.menuGET = {'get serviceID': self.getService, 'get broker': self.getBroker}
		self.menuPUTPOST = {'register serviceID': self.registerService }
		self.broker = { 'broker': broker, 'port': port }
		self.serviceListLock = lock
	
	def GET (self, *uri, **param):
		command = ' '.join(uri)
		try:
			output = self.menuGET[command](list(param.values())[0])
		except KeyError:
			output = 'Wrong request'
		except IndexError:
			output = self.menuGET[command]()
		return output 
	def POST (self, *uri, **param):
		es = list(uri)
		command = ' '.join(uri)
		payload = json.loads(cherrypy.request.body.read())
		try:
			output = self.menuPUTPOST[command](payload, True)
		except KeyError:
			output = 'Wrong request'
		except CustomError as error:
			output = error.message
		return output

	def PUT (self, *uri, **param):
		res = list(uri)
		command = ' '.join(uri)
		payload = json.loads(cherrypy.request.body.read())
		try:
			output = self.menuPUTPOST[command](payload, False)
		except KeyError:
			output = 'Wrong request'
		return output 
	
	def getBroker(self):
		return json.dumps(self.broker, indent = 4)
	
	def getService(self, serviceID = None):
		found = 'serviceID not found'
		## entering critical section
		self.serviceListLock.acquire()
		if serviceID!= None:
			for i in range(len(self.serviceList)):
				if self.serviceList[i]['serviceID'] == serviceID:
					found = json.dumps(self.serviceList[i], indent = 4)
		else:
			found = json.dumps(self.serviceList, indent = 4)
		self.serviceListLock.release()
		## exiting critical section
		return found

	
	def registerService(self, *data):
		serviceData = data[0]
		isPOST = data[1]
		serviceData['insert-timestamp'] = time.time()
		output = 'Service registered'
		## entering critical section
		self.serviceListLock.acquire()
		try:
			for i in range(len(self.serviceList)):
				if self.serviceList[i]['serviceID'] == serviceData['serviceID']:
					if isPOST:
						raise CustomError('ServiceID is not available')
					else:
						self.serviceList.pop(i)
						output = 'Service updated'
						break
			self.serviceList.append(serviceData)
		except KeyError:
			output = "ServiceID is missing"
		finally:
			self.serviceListLock.release()
			## exiting critical section
			return output



class refreshThread(threading.Thread):
	def __init__(self, threadID, catalog):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.catalog = catalog
	def run(self):

		while keepAlive:
			time.sleep(20)
		## entering critical section
			self.catalog.serviceListLock.acquire()
			for i in range(len(self.catalog.serviceList)):
				if time.time()-self.catalog.serviceList[i]['insert-timestamp'] > 20:
					self.catalog.serviceList.pop(i)
					break
			print(f'{json.dumps(self.catalog.serviceList, indent = 4)}')
			self.catalog.serviceListLock.release()
		## exiting critical section
		
if __name__=='__main__':
	print(f'{os.getpid()}')
	userData = 'users.json'
	confData = 'conf.json'
	keepAlive = True
	with open(confData, 'r') as configuration:
		config = json.load(configuration)
		configuration.close()
	
	
	deviceListLock = threading.Lock()
	catalog = ServiceCatalog( 'catalog1', config['broker'], config['port'], userData,deviceListLock)
	conf = {
		'/':{
				'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
				'tools.sessions.on': True 
		}
	}
	thread1 = refreshThread('thread1', catalog)
	#external iterface 
    #'http://192.168.1.9:8080/....9091'
	cherrypy.config.update({'server.socket_host': '192.168.1.19','server.socket_port':8080})
	cherrypy.tree.mount(catalog,'/', conf)
	cherrypy.engine.start()
	thread1.start()
	cherrypy.engine.block()
	keepAlive = False 