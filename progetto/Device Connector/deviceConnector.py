from myMQTT import *
import time
import json
import threading
import os
import requests




class DeviceBooleanSlave():
    def __init__(self, clientID, broker, topic, port, deviceData):
        self.client=MyMQTT(clientID, broker, port,self)
        self.topic=topic
        self.port=port
        self.broker=broker
        self.clientID=clientID
        self.status = 0
        self.deviceData = deviceData
        
        
    def notify(self,topic,msg):
        r = json.loads(msg)
        self.status = r['newStatus']
    def start(self):
        self.client.start()
    def stop(self):
        self.client.stop()

class DeviceStringMaster():
    def __init__(self, clientID, broker, topic, port,deviceData):
        self.client=MyMQTT(clientID, broker, port)
        self.topic=topic
        self.port=port
        self.broker=broker
        self.clientID=clientID
        self.deviceData = deviceData
    def setValue(self, value):
        d = {"value": value, "id": self.clientID, "time": time.time()}
        self.client.myPublish(self.topic, d)
    def start(self):
        self.client.start()
    def stop(self):
        self.client.stop()
class masterDevThread(threading.thread):
    def __init__(self, device):
        threading.Thread.__init__(self)
        self.device = device
    def run(self):
        with open('strings.json', 'r') as data:
            rfidList = json.load(data)
            data.close()
        for i in range(len(rfidList)):
            self.device.setValue(rfidList[i]['str'])
            time.sleep(100)

class RegistrationThread(threading.Thread):
    def __init__(self,deviceListLock, serviceCatalogUrl, deviceList):
        threading.Thread.__init__(self)
        self.deviceListLock = deviceListLock
        self.serviceCatalogUrl = serviceCatalogUrl
        self.deviceList = deviceList
    def run(self):
        while True:
            time.sleep(10)
            #find device Catalog
            payload = { 'serviceID': 'Device Catalog'}
            r = requests.get(self.serviceCatalogUrl+'get/serviceID?serviceID=Device+Catalog')
            #print(f'{r.text}')
            deviceCatalog = json.loads(r.text)
            #enter critical section
            self.deviceListLock.acquire()

            for i in range(len(self.deviceList)):
                r = requests.put(deviceCatalog['url']+'add/deviceID', data = json.dumps(self.deviceList[i].deviceData))

            self.deviceListLock.release()
            #exit critical section
class DiscoverThread(threading.Thread):
    def __init__(self,deviceListLock, deviceList, brokerData):
        threading.Thread.__init__(self)
        self.deviceListLock = deviceListLock
        self.deviceList = deviceList
        self.brokerData = brokerData
    def run(self):
        menu = dict(a = self.add, r = self.remove)
        while True:
            req = input('a-insert device\nr-remove device\n')
            data = input('insert data')
            menu[req](data)
            
    def add(self,d):
        data = json.loads(d)
        if data['type'] == 'stringMaster':
            newDev = DeviceStringMaster(data['deviceID'], self.brokerData['broker'], data['deviceID'],self.brokerData['port'] , data)
            
        elif data['type'] == 'booleanSlave':
            newDev = DeviceBooleanSlave(data['deviceID'], self.brokerData['broker'], data['deviceID'],self.brokerData['port'] , data)
        
        #entering critical section
        self.deviceListLock.acquire()
        for i in range(len(self.deviceList)):
            if(self.deviceList[i].deviceData['deviceID'] == data['deviceID']):
                self.deviceList[i].stop()
                self.deviceList.pop(i)
                print('trovato!!')
        self.deviceList.append(newDev)
        self.deviceListLock.release()
        #exit critical section 
        print(f'{json.dumps(newDev.deviceData)}')
    def remove(self,d):
        data = json.loads(d)
        newDev = DeviceBooleanSlave(data['deviceID'], self.brokerData['broker'], data['deviceID'],self.brokerData['port'] , data)
        #entering critical section
        self.deviceListLock.acquire()
        for i in range(len(self.deviceList)):
            if(self.deviceList[i]['deviceID'] == data):
                self.deviceList[i].stop()
                self.deviceList.pop(i)
        self.deviceListLock.release()
        #exit critical section


if __name__=='__main__':
    deviceList = []
    serviceCatalogUrl = 'http://127.0.0.1:8080/'
    lock = threading.Lock()
    r = requests.get(serviceCatalogUrl+'get/broker')
    config = json.loads(r.text)
    thread1 = RegistrationThread(lock, serviceCatalogUrl, deviceList)
    thread2 = DiscoverThread(lock, deviceList, config)
    
    thread2.start()
    thread1.start()




