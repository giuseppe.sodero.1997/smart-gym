from myMQTT import *
import time
import json
import threading
import cherrypy
import requests
import socket
import datetime
url_servicecatalog  = 'http://192.168.1.19:8080/'
s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
s.connect(("8.8.8.8",80))
localIp = s.getsockname()[0]
socketPort = 60060


cherrypy.config.update({'server.socket_port': 60060})

class CustomError(Exception):
	def __init__(self, message):
		Exception.__init__(self)
		self.message = message


class ThingspeakAdapter():
    exposed = True
    def __init__(self, clientID, serviceCatalogUrl, url):
        r = requests.get(serviceCatalogUrl+'get/broker')
        while r.status_code != requests.codes.ok:
            time.sleep(5)
            r = requests.get(serviceCatalogUrl+'get/broker')
        brokerData = json.loads(r.text)   
        self.clientID = clientID
        self.broker = brokerData['broker']
        self.port = brokerData['port']
        self.client = MyMQTT(clientID, self.broker, self.port,self)
        brokerData = json.loads(r.text)
        self.url = url 
        self.serviceCatalogUrl = serviceCatalogUrl
        self.machineBuffer = {}
        self.lock = threading.Lock()
        self.updateThread = UpdateThread('update Thread1', self)
        self.thread_reg = refreshThread("thread_reg", self)
    def start(self):
        self.client.start()

        self.client.mySubscribe('IOT/+/+/INFO')
        self.client.mySubscribe('IOT/+/+/+/+/INFO')
        self.thread_reg.start() 
        self.updateThread.start()
    def PUT():
        pass
    def GET(self, *uri, **param):
        if list(uri)[0] == "machine":
            r = requests.get("https://api.thingspeak.com/channels/1663105/fields/1.json?api_key=KETKAGZ7J36YG9YF&results=2")
            if r.status_code == requests.codes.ok:
                print("data retreived from thingspeak\n")
                return r.text
            else:
                print("an error occurred")
                return ""
    def stop(self):
        self.client.stop
        self.updateThread.join()
        self.thread_reg.join()
    def notify(self,topic,msg):
        data = json.loads(msg)
        entry = dict(machineTopic = topic, status = data, timeStamp = time.time(), dateTime = str(datetime.datetime.now()))
        if len(topic.split('/')) == 4:
            pass
        elif len(topic.split('/')) == 6:
            print(data)
            #entering critical section
            self.lock.acquire()
            if self.machineBuffer.get(topic, None) != None:
                self.machineBuffer[topic].append(entry)
            else:
                self.machineBuffer[topic] = []
                self.machineBuffer[topic].append(entry)
           
            
            self.lock.release()
            #exit critical section
        else:
            pass
        print(f'topic is: {topic}, msg = {data}')

class UpdateThread(threading.Thread):
    def __init__(self, threadID, client):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.client = client
    def run(self):
        while True:
            #entering critical section
            self.client.lock.acquire()
           
            for key in self.client.machineBuffer.keys():
                #checking if last message sent to the channle is more than 15s old
                if len(self.client.machineBuffer[key])>0:
                    if time.time()-self.client.machineBuffer[key][0]['timeStamp']  >= 15:
                        print(json.dumps(self.client.machineBuffer[key][0]))
                        #getting informations about machine data channel
                        machineData = None
                        r = requests.get(self.client.serviceCatalogUrl+'get/serviceID?serviceID=Device+Catalog')
                        if r.status_code == requests.codes.ok:
                            if r.text != 'serviceID not found':
                                catalogInfo = json.loads(r.text)
                                print(key)
                                r = requests.get(catalogInfo['url']+'get/deviceID?deviceID='+key.split('/')[4])
                                if r.status_code == requests.codes.ok:
                                    machineData = json.loads(r.text)
                            if machineData != None:
                                r = requests.get(machineData['writeAPI']+json.dumps(self.client.machineBuffer[key][0]))
                                if r.status_code == requests.codes.ok:
                                    print("data sent to thingspeak\n")
                                    self.client.machineBuffer[key].pop(0)
                                else:
                                    print("an error occurred")
            self.client.lock.release()
            time.sleep(15)
            #exit critical section


class refreshThread(threading.Thread):
    def __init__(self, threadID, client):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.client = client
    def run(self):
        while True:
            time.sleep(5)
            registration_payload={
                "serviceID":"thinspeak_adapter",
                "url": self.client.url
            }
            r = requests.put(self.client.serviceCatalogUrl + "register serviceID", data=json.dumps(registration_payload))
        



if __name__ == "__main__":
    conf = {
        '/':{
            'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on':True
        }
    }
    cherrypy.config.update({'server.socket_host': localIp,'server.socket_port':socketPort})
    adapter=ThingspeakAdapter('adapter3',url_servicecatalog, 'http://192.168.1.19:60060/')
    thread_reg = refreshThread("thread_reg", adapter)
    cherrypy.tree.mount(adapter, '/', conf)
    cherrypy.engine.start()
    thread_reg.start()
    adapter.start()
    cherrypy.engine.block()