from myMQTT import *
import time
import json
import threading
import cherrypy
import requests

class CustomError(Exception):
	def __init__(self, message):
		Exception.__init__(self)
		self.message = message

class Machine():
    exposed = True
    def __init__(self, clientID, broker, port, serviceCatalogUrl,timeCicle, address, gym, room):
        self.client=MyMQTT(clientID, broker, port,self)
        self.port=port
        self.broker=broker
        self.clientID=clientID
        self.lock = threading.Lock()
        self.serviceCatalogUrl = serviceCatalogUrl
        self.timeCicle = timeCicle
        self.topicSet = set()
        self.waiting = False
        self.status = 0
        self.handshakeSemaphore = threading.Semaphore()
        self.registration = RegistrationThread('registration1', self.serviceCatalogUrl, self)
        self.registration.start()
        self.address = address
        self.gym = gym
        self.room = room  
        self.baseTopic =   'IOT/'+ gym +'/'+ room + '/MACHINE/'+ clientID
        self.machineTopic = self.baseTopic + '/STATUS'
        self.infoTopic = self.baseTopic + '/info' 
        self.active = True
        self.msg ={
            'bn':self.clientID,
            'e':[
                    {
                        'url': self.address,
                        'handshake': None,
                        'status': None,
                        'active':None,
                        'set': None
                    }
                ]
            }
    
    def start(self):
        try:
            ############################################################################################### owner Dashboard request
#            r = requests.get(self.serviceCatalogUrl+'get/serviceID?serviceID=Owner+dashboard')
#            if r != 'serviceID not found':
#                ownerDashboard = json.loads(r.text) 
#                r = requests.get(ownerDashboard + 'machineConfiguration?deviceID='+self.clientID+'&machineTopic='+self.baseTopic)
#                data = json.loads(r.text)
#                if data.get('topics', None) == None:
#                    raise CustomError
#                else:
#                    newTopicSet = set(data['topics'])
            
            ######################################################################################################### debug version
            groupFile = input('insert Grup file name\n') 
            with open(groupFile, 'r') as fp:
                data = json.load(fp)
                newTopicSet = set(data['topics'])
                fp.close
            #########################################################################################################
            self.client.start()
            for i in newTopicSet:
                self.client.mySubscribe(i)
            self.topicSet = newTopicSet
            userArriving = inputThread('threadInput1', self)
            userArriving.start()
            output = 'configuration completed'
        except CustomError:
            output = 'configuration failed'
        return output


    
    def notify(self,topic,msg):
        #entering Critical Section
        
        self.lock.acquire()
        data = json.loads(msg)
        t = { self.machineTopic }
        topicSection = topic.split('/')
        if topicSection[-1] == 'STATUS':
            if t.issubset(set(data['e'][0].get('set', []))):
                handshake = data['e'][0].get('handshake', None)
                if handshake == 'connection':
                    self.status = 2
                    r = requests.put(data['e'][0]['url']+'Connection', data = "ok" )
                    self.client.myPublish(self.infoTopic, self.status)
                else: 
                    if handshake == 'release':
                        self.status = 0
                        self.client.myPublish(self.infoTopic, self.status)
        elif topicSection[-1] == 'UPDATE':
            payload = json.loads(msg)
            newTopicSet = set(payload['topics'])
            machines2remove = self.machine.topicSet.difference(newTopicSet)
            newMachines = newTopicSet.difference(self.machine.topicSet)
            for i in machines2remove:
                self.machine.client.unsubscribe(i)
                self.machine.topicSet.remove(i)
            for i in newMachines:
                self.machine.client.mySubscribe(i)
                self.machine.topicSet.add(i)     
        
        self.lock.release()
        #exit critical section
    
    def PUT(self,*uri, **params):
        r = list(uri)
        try:
            command = r[0]
        
        except IndexError:
            return    
    
        if command == 'Connection':
            if self.waiting:
                self.handshakeSemaphore.release()
        if command == 'switch':
            self.active = not self.active
        if command == 'configuration':
            try:
                #entering critical section
                self.lock.acquire()
                data = json.loads(cherrypy.request.body.read())
                if data.get('topics', None) == None:
                    raise CustomError
                else:
                    newTopicSet = set(data['topics'])
                for i in self.topicSet:
                    self.client.unsubscribe(i)
            
                for i in newTopicSet:
                    self.client.mySubscribe(i)
                self.topicSet = newTopicSet
                output = 'configuration done'
            except CustomError:
                output = 'Wrong Configuration Dictionary'
            finally:    
                self.lock.release()
                #exit critical section
            return output 
        
    def RFIDdetection(self):
        RFID = input('insert RFID\n')
        ## database requests and verification
        idOk = True
        re = requests.get(self.serviceCatalogUrl+'get/serviceID?serviceID=entry_control')
        rd = requests.get(self.serviceCatalogUrl+'get/serviceID?serviceID=user_database')
        if re.text != 'service not found' and rd.text:
            entryControl = json.loads(re.text)
            userDatabase = json.loads(rd.text)
        rd = requests.get(userDatabase['url']+'get/userID?userID='+ RFID)
        re = requests.get(entryControl['url']+'get/userID?userID='+ RFID +'&roomTopic=IOT/'+self.gym + '/' + self.room )
        if re.text == 'UserID not found' or rd.text == 'UserID not found':
            idOk = False
        ##entering critical section
        self.lock.acquire()
        if self.status == 0 and self.active and idOk:
            self.waiting = True
            msg = self.msg.copy()
            msg['e'][0]['handshake'] = 'connection'
            msg['e'][0]['status'] = 1
            msg['e'][0]['active'] = self.active
            msg['e'][0]['set'] = list(self.topicSet)
            self.client.myPublish(self.machineTopic, msg)
            self.handshakeSemaphore = threading.Semaphore()
            self.handshakeSemaphore.acquire()
            for i in range(len(self.topicSet)):
                gotResponse = self.handshakeSemaphore.acquire(timeout=5)
                if not gotResponse:
                    break
            if gotResponse:
                print(f'ok! user {RFID} activated machine{self.clientID}')
                self.status = 1
                self.client.myPublish(self.infoTopic, self.status)
                time.sleep(self.timeCicle)
                self.status = 0
            else:
                print("connection to neighborhood lost, access deneid")
            msg = self.msg.copy()
            msg['e'][0]['handshake'] = 'release'
            msg['e'][0]['status'] = 0
            msg['e'][0]['active'] = self.status
            msg['e'][0]['set'] = list(self.topicSet)
            self.client.myPublish(self.machineTopic, msg)
            self.client.myPublish(self.infoTopic, self.status)
        else: 
            print('Machine is Unavailable, access denied')
        self.lock.release()
        ##exit critical section
    

class RegistrationThread(threading.Thread):
    def __init__(self, threadID, serviceCatalogUrl, machine):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.serviceCatalogUrl = serviceCatalogUrl
        self.machine = machine
    def run(self):
        while True:
            r = requests.get(self.serviceCatalogUrl+'get/serviceID?serviceID=Device+Catalog')
            if r.text != 'service not found':
                catalogInfo = json.loads(r.text)
                info = dict(deviceID=self.machine.client.clientID, topic = self.machine.machineTopic, infoTopic = self.machine.infoTopic, url = self.machine.address)
                r = requests.put(catalogInfo['url']+'add/deviceID', data=json.dumps(info))
            time.sleep(5)

class inputThread(threading.Thread):
    def __init__(self, threadID, machine):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.machine = machine
    def run(self):
        while True:
            self.machine.RFIDdetection()





if __name__=='__main__':
    serviceCatalogUrl = 'http://127.0.0.1:8080/'
    r = requests.get(serviceCatalogUrl+"get broker")
    config = json.loads(r.text)
    
    filePath = input('Insert configuration file name\n')
    with open(filePath, 'r') as confMachine:
        machineData = json.load(confMachine)
        confMachine.close()      
    machine = Machine(machineData['clientID'], config['broker'], config['port'], serviceCatalogUrl,10, machineData['address'], machineData['gym'], machineData['room'])
    conf = {
		'/':{
				'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
				'tools.sessions.on': True
		}
	}

    cherrypy.config.update({'server.socket_port': machineData['port']})
    cherrypy.tree.mount(machine,'/', conf)
    cherrypy.engine.start()
    while r != 'configuration completed':
        r = machine.start()
    cherrypy.engine.block()


