import binascii

from pn532pi import Pn532, pn532
from pn532pi import Pn532I2c

def setup():
    global nfc
    PN532_I2C = Pn532I2c(1)
    nfc = Pn532(PN532_I2C)
    nfc.begin()
    versiondata = nfc.getFirmwareVersion()
    if (not versiondata):
        print("Didn't find PN53x board")
        raise RuntimeError("Didn't find PN53x board")  # halt
    nfc.SAMConfig()

def run():
    success, uid = nfc.readPassiveTargetID(pn532.PN532_MIFARE_ISO14443A_106KBPS)
    if (success):
        uidDec = binascii.hexlify(uid).decode('utf-8')
        return True
    return False

if __name__ == '__main__':
    setup()
    found = run()
    while not found:
        found = run()